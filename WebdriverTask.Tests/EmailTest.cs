﻿using OpenQA.Selenium.Edge;
using OpenQA.Selenium;

namespace WebdriverTask.Tests
{
    [TestClass]
    public class EmailTest
    {
        public const string emailMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra justo id porttitor tincidunt.";
        public const string emailSubject = "The webdriver task";
        public const string inboxEmail = username + "@inbox.lv";
        public const string inboxNotificationText = "Jūsu vēstule ir veiksmīgi nosūtīta un saglabāta mapē Aizsūtītie";
        public const string newAlias = "TaskFinished";
        public const string protonNotificationText = "Message sent";
        public const string password = "WebdriverTask123";
        public const string protonEmail = username + "@proton.me";
        public const string username = "webdrivertask";
        public IWebDriver Driver { get; set; }
        public InboxMainPage InboxMainPage { get; set; }
        public ProtonMainPage ProtonMainPage { get; set; }

        [TestInitialize]
        public void EdgeDriverInitialize()
        {
            Driver = new EdgeDriver();
            InboxMainPage = new InboxMainPage(Driver);
            ProtonMainPage = new ProtonMainPage(Driver);
        }

        [TestCleanup]
        public void EdgeDriverCleanup()
        {
            Driver.Quit();
        }

        [TestMethod]
        public void EmailTest_ExchangeOfEmailsAndNameChange_AreSuccessful()
        {
            ProtonMainPage.Navigate();
            ProtonMainPage.LogIn(username, password);
            ProtonMainPage.Validate().SuccessfulLogin();
            ProtonMainPage.WriteEmail(inboxEmail, emailSubject, emailMessage);
            ProtonMainPage.SendEmail();
            ProtonMainPage.Validate().EmailSentNotificationText(protonNotificationText);
            InboxMainPage.Navigate();
            InboxMainPage.Refresh();
            InboxMainPage.LogIn(username, password);
            InboxMainPage.Validate().WaitForIncomingMail();
            InboxMainPage.OpenNewestEmail();
            InboxMainPage.Validate().CorrectSenderEmail(protonEmail);
            InboxMainPage.Validate().CorrectEmailContent(emailMessage);
            InboxMainPage.WriteEmail(protonEmail, emailSubject, newAlias);
            InboxMainPage.SendEmail();
            InboxMainPage.Validate().EmailSentNotificationText(inboxNotificationText);
            ProtonMainPage.Navigate();
            ProtonMainPage.Validate().SuccessfulLogin();
            ProtonMainPage.EditDisplayName(newAlias);
            ProtonMainPage.Validate().DisplayNameText(newAlias);
        }
    }
}
