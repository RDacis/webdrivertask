﻿using OpenQA.Selenium.Edge;
using OpenQA.Selenium;

namespace WebdriverTask.Tests
{
    [TestClass]
    public class LoginTests
    {
        public const string inboxErrorMessage = "Ieeja nav izdevusies. Acīmredzot lietotāja vārds vai parole bija norādīti nepareizi.";
        public const string password = "WebdriverTask123";
        public const string protonErrorMessage = "This field is required";
        public const string username = "webdrivertask";
        public const string wrongPassword = "TaskWebDriver321";
        public const string wrongUsername = "taskwebdr1ver";
        public IWebDriver Driver { get; set; }
        public InboxMainPage InboxMainPage { get; set; }
        public ProtonMainPage ProtonMainPage { get; set; }

        [TestInitialize]
        public void EdgeDriverInitialize()
        {
            Driver = new EdgeDriver();
            InboxMainPage = new InboxMainPage(Driver);
            ProtonMainPage = new ProtonMainPage(Driver);
        }

        [TestCleanup]
        public void EdgeDriverCleanup()
        {
            Driver.Quit();
        }

        [TestMethod]
        public void ErrorMessageText_WithEmptyUsernameAndPassword_ShowsCorrectErrorMessage()
        {
            ProtonMainPage.Navigate();
            ProtonMainPage.LogIn(string.Empty, string.Empty);
            ProtonMainPage.Validate().UsernameErrorMessageText(protonErrorMessage);
            ProtonMainPage.Validate().PasswordErrorMessageText(protonErrorMessage);
        }

        [TestMethod]
        public void ErrorMessageText_WithEmptyUsername_ShowsCorrectErrorMessage()
        {
            ProtonMainPage.Navigate();
            ProtonMainPage.LogIn(string.Empty, password);
            ProtonMainPage.Validate().UsernameErrorMessageText(protonErrorMessage);
            ProtonMainPage.Validate().PasswordErrorMessageNotDisplayed(protonErrorMessage);
        }

        [TestMethod]
        public void ErrorMessageText_WithEmptyPassword_ShowsCorrectErrorMessage()
        {
            ProtonMainPage.Navigate();
            ProtonMainPage.LogIn(username, string.Empty);
            ProtonMainPage.Validate().UsernameErrorMessageNotDisplayed(protonErrorMessage);
            ProtonMainPage.Validate().PasswordErrorMessageText(protonErrorMessage);
        }

        [TestMethod]
        public void ErrorMessageText_WithIncorrectUsername_ShowsCorrectErrorMessage()
        {
            InboxMainPage.Navigate();
            InboxMainPage.Refresh();
            InboxMainPage.LogIn(wrongUsername, password);
            InboxMainPage.Validate().ErrorMessageText(inboxErrorMessage);
        }

        [TestMethod]
        public void ErrorMessageText_WithIncorrectPassword_ShowsCorrectErrorMessage()
        {
            InboxMainPage.Navigate();
            InboxMainPage.Refresh();
            InboxMainPage.LogIn(username, wrongPassword);
            InboxMainPage.Validate().ErrorMessageText(inboxErrorMessage);
        }

        [TestMethod]
        public void SuccessfulLogin_WithCorrectCredentials_LogsUserIn()
        {
            ProtonMainPage.Navigate();
            ProtonMainPage.LogIn(username, password);
            ProtonMainPage.Validate().SuccessfulLogin();
        }
    }
}