﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace WebdriverTask
{
    public class InboxMainPage
    {
        private readonly IWebDriver browser;
        private readonly string url = @"https://www.inbox.lv";
        public InboxMainPage(IWebDriver browser)
        {
            this.browser = browser;
        }
        protected InboxMainPageElementMap Map
        {
            get
            {
                return new InboxMainPageElementMap(browser);
            }
        }
        public InboxMainPageValidator Validate()
        {
            return new InboxMainPageValidator(browser);
        }
        public void Navigate()
        {
            browser.Navigate().GoToUrl(url);
        }
        public void Refresh()
        {
            browser.Navigate().Refresh();
        }
        public void LogIn(string username, string password)
        {
            Map.Username.Clear();
            Map.Username.SendKeys(username);
            Map.Password.Clear();
            Map.Password.SendKeys(password);
            Map.SubmitButton.Click();
        }
        public void OpenNewestEmail()
        {
            Map.NewestEmailReceived.Click();
        }
        public void WriteEmail(string emailRecipient, string emailSubject, string emailMessage)
        {
            Map.NewMessageButton.Click();
            Map.EmailRecipient.Clear();
            Map.EmailRecipient.SendKeys(emailRecipient);
            Map.EmailSubject.Clear();
            Map.EmailSubject.SendKeys(emailSubject);
            browser.SwitchTo().Frame(1);
            Map.EmailEditor.Clear();
            Map.EmailEditor.SendKeys(emailMessage);
            browser.SwitchTo().DefaultContent();
        }
        public void SendEmail()
        {
            Map.SendEmailButton.Click();
        }
    }
}
