﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace WebdriverTask
{
    public class InboxMainPageValidator
    {
        private readonly IWebDriver browser;
        public InboxMainPageValidator(IWebDriver browser)
        {
            this.browser = browser;
        }
        protected InboxMainPageElementMap Map
        {
            get
            {
                return new InboxMainPageElementMap(browser);
            }
        }
        public void ErrorMessageText(string errorMessage)
        {
            Assert.IsTrue(Map.ErrorMessageDiv.Text.Contains(errorMessage), "The error message doesn't contain the specified text.");
        }
        public void SuccessfulLogin()
        {
            Assert.IsTrue(Map.MailboxContainer.Displayed, "The login was not successful.");
        }
        public void WaitForIncomingMail()
        {
            Thread.Sleep(TimeSpan.FromSeconds(45));
            browser.Navigate().Refresh();
            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(60));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id=\"side-trigger\"]/span[1]/span")));
        }
        public void CorrectSenderEmail(string expectedEmail)
        {
            Assert.IsTrue(Map.SenderEmail.Text.Contains(expectedEmail), "The email was sent by a different person.");
        }
        public void CorrectEmailContent(string expectedMessage)
        {
            Assert.IsTrue(Map.EmailContent.Text.Contains(expectedMessage), "The email contains a different message.");
        }
        public void EmailSentNotificationText(string notificationText)
        {
            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(20));
            IWebElement notification = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".alert.alert-success.alert_has-icon.alert-dismissible")));
            wait.Until(ExpectedConditions.TextToBePresentInElement(notification, notificationText));
        }
    }
}
