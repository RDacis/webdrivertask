﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace WebdriverTask
{
    public class ProtonMainPageValidator
    {
        private readonly IWebDriver browser;
        public ProtonMainPageValidator(IWebDriver browser)
        {
            this.browser = browser;
        }
        protected ProtonMainPageElementMap Map
        {
            get
            {
                return new ProtonMainPageElementMap(browser);
            }
        }
        public void UsernameErrorMessageText(string usernameErrorMessage)
        {
            Assert.IsTrue(Map.UsernameErrorMessage.Text.Contains(usernameErrorMessage), "The error message doesn't contain the specified text.");
        }
        public void UsernameErrorMessageNotDisplayed(string usernameErrorMessage)
        {
            Assert.IsFalse(Map.UsernameErrorMessage.Text.Contains(usernameErrorMessage), "The error message doesn't contain the specified text.");
        }
        public void PasswordErrorMessageText(string passwordErrorMessage)
        {
            Assert.IsTrue(Map.PasswordErrorMessage.Text.Contains(passwordErrorMessage), "The error message doesn't contain the specified text.");
        }
        public void PasswordErrorMessageNotDisplayed(string passwordErrorMessage)
        {
            Assert.IsFalse(Map.PasswordErrorMessage.Text.Contains(passwordErrorMessage), "The error message doesn't contain the specified text.");
        }
        public void SuccessfulLogin()
        {
            Assert.IsTrue(Map.NewMessageButton.Text.Contains("New message"), "The login was not successful.");
        }
        public void DisplayNameText(string displayName)
        {
            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.TextToBePresentInElement(Map.DisplayName, displayName));
        }
        public void EmailSentNotificationText(string notificationText)
        {
            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(20));
            IWebElement notification = wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(".notification.notification--in.notification--success.notification--has-close-button")));
            wait.Until(ExpectedConditions.TextToBePresentInElement(notification, notificationText));
        }
        public void SuccessfulSignOut()
        {
            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("username")));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("password")));
        }
    }
}
