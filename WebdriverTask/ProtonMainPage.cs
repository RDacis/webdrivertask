﻿using OpenQA.Selenium;

namespace WebdriverTask
{
    public class ProtonMainPage
    {
        private readonly IWebDriver browser;
        private readonly string url = @"https://account.proton.me/mail";
        public ProtonMainPage(IWebDriver browser)
        {
            this.browser = browser;
        }
        protected ProtonMainPageElementMap Map
        {
            get
            {
                return new ProtonMainPageElementMap(browser);
            }
        }
        public ProtonMainPageValidator Validate()
        {
            return new ProtonMainPageValidator(browser);
        }
        public void Navigate()
        {
            browser.Navigate().GoToUrl(url);
        }
        public void LogIn(string username, string password)
        {
            browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            Map.Username.Clear();
            Map.Username.SendKeys(username);
            Map.Password.Clear();
            Map.Password.SendKeys(password);
            Map.SubmitButton.Click();
        }
        public void WriteEmail(string emailRecipient, string emailSubject, string emailMessage)
        {
            browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            Map.NewMessageButton.Click();
            Map.EmailRecipient.Clear();
            Map.EmailRecipient.SendKeys(emailRecipient);
            Map.EmailSubject.Clear();
            Map.EmailSubject.SendKeys(emailSubject);
            browser.SwitchTo().Frame(0);
            Map.EmailEditor.Clear();
            Map.EmailEditor.SendKeys(emailMessage);
            browser.SwitchTo().DefaultContent();
        }
        public void SignOut()
        {
            browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            Map.AccountButton.Click();
            Map.SignOutButton.Click();
        }
        public void SendEmail()
        {
            browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            Map.SendEmailButton.Click();
        }
        public void EditDisplayName(string newDisplayName)
        {
            browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            Map.SettingsButton.Click();
            Map.AllSettingsButton.Click();
            Map.IdentityAndAddressesLink.Click();
            Map.DisplayNameInput.Clear();
            Map.DisplayNameInput.SendKeys(newDisplayName);
            Map.UpdateDisplayNameButton.Click();
        }
    }
}
