﻿using OpenQA.Selenium;

namespace WebdriverTask
{
    public class InboxMainPageElementMap
    {
        private readonly IWebDriver browser;
        public InboxMainPageElementMap(IWebDriver browser)
        {
            this.browser = browser;
        }
        public IWebElement Username
        {
            get
            {
                return browser.FindElement(By.Id("imapuser"));
            }
        }
        public IWebElement Password
        {
            get
            {
                return browser.FindElement(By.Id("pass"));
            }
        }
        public IWebElement SubmitButton
        {
            get
            {
                return browser.FindElement(By.Id("btn_sign-in"));
            }
        }
        public IWebElement ErrorMessageDiv
        {
            get
            {
                return browser.FindElement(By.Id("flash-messages"));
            }
        }
        public IWebElement MailboxContainer
        {
            get
            {
                return browser.FindElement(By.Id("mailbox-container"));
            }
        }
        public IWebElement SenderEmail
        {
            get
            {
                return browser.FindElement(By.XPath("//*[@id=\"eml-read__headers\"]/div[1]/div/div/address/span"));
            }
        }
        public IWebElement NewestEmailReceived
        {
            get
            {
                return browser.FindElement(By.XPath("//*[@id=\"maillist-container\"]/div[1]/a"));
            }
        }
        public IWebElement EmailContent
        {
            get
            {
                return browser.FindElement(By.Id("messageText"));
            }
        }
        public IWebElement NewMessageButton
        {
            get
            {
                return browser.FindElement(By.Id("mail-menu__link_compose"));
            }
        }
        public IWebElement EmailRecipient
        {
            get
            {
                return browser.FindElement(By.Id("suggest-to"));
            }
        }
        public IWebElement EmailSubject
        {
            get
            {
                return browser.FindElement(By.Id("subject"));
            }
        }
        public IWebElement EmailEditor
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body"));
            }
        }
        public IWebElement SendEmailButton
        {
            get
            {
                return browser.FindElement(By.XPath("//*[@id=\"eml-toolbar-container\"]/div/div[1]/button[2]"));
            }
        }
    }
}
