﻿using OpenQA.Selenium;

namespace WebdriverTask
{
    public class ProtonMainPageElementMap
    {
        private readonly IWebDriver browser;
        public ProtonMainPageElementMap(IWebDriver browser)
        {
            this.browser = browser;
        }
        public IWebElement Username
        {
            get
            {
                return browser.FindElement(By.Id("username"));
            }
        }
        public IWebElement Password
        {
            get
            {
                return browser.FindElement(By.Id("password"));
            }
        }
        public IWebElement SubmitButton
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[4]/div[1]/main/div[1]/div[2]/form/button"));
            }
        }
        public IWebElement UsernameErrorMessage
        {
            get
            {
                return browser.FindElement(By.Id("id-3"));
            }
        }
        public IWebElement PasswordErrorMessage
        {
            get
            {
                return browser.FindElement(By.Id("id-4"));
            }
        }
        public IWebElement NewMessageButton
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[1]/div[3]/button"));
            }
        }
        public IWebElement EmailRecipient
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/div/div/div[2]/div/div/div/div/div/div/input"));
            }
        }
        public IWebElement EmailSubject
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/div/div/div[3]/div/div/input"));
            }
        }
        public IWebElement EmailEditor
        {
            get
            {
                return browser.FindElement(By.Id("rooster-editor"));
            }
        }
        public IWebElement SendEmailButton
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[4]/div/div/div/footer/div/div[1]/button[1]"));
            }
        }
        public IWebElement AccountButton
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/header/div[2]/ul/li[3]/button"));
            }
        }
        public IWebElement SignOutButton
        {
            get
            {
                return browser.FindElement(By.XPath("//*[@id=\"dropdown-34\"]/div[2]/ul/li[5]/div/button"));
            }
        }
        public IWebElement SettingsButton
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div[2]/div/div[2]/div/div/div/header/div[2]/ul/li[2]/button"));
            }
        }
        public IWebElement AllSettingsButton
        {
            get
            {
                return browser.FindElement(By.XPath("//*[@id=\"drawer-app-proton-settings\"]/div[2]/div/div[3]/div/div/a"));
            }
        }
        public IWebElement IdentityAndAddressesLink
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div/div[1]/div[5]/nav/ul/ul[2]/li[4]/a"));
            }
        }
        public IWebElement DisplayNameInput
        {
            get
            {
                return browser.FindElement(By.Id("displayName"));
            }
        }
        public IWebElement UpdateDisplayNameButton
        {
            get
            {
                return browser.FindElement(By.XPath("//*[@id=\"name-signature\"]/div/form/div[2]/div[2]/button"));
            }
        }
        public IWebElement DisplayName
        {
            get
            {
                return browser.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div/div/div[2]/div/div/header/div[2]/ul/li[2]/button/span[1]/span[1]"));
            }
        }
    }
}
